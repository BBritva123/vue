function productEnding(count) {
  let ending = String(count).split('')
  let secEnd = ending.slice(-2, -1)
  function end() {
    if (ending.slice(-1)[0] === '1') {
      return 'one'
    } else if (ending.slice(-1)[0] === '0' || Number(ending.slice(-1)[0]) <= 9 && Number(ending.slice(-1)[0]) >= 5) {
      return 'manyof'
    } else {
      return 'many'
    }
  }
  function defineEnding() {
    if (ending.length >= 2) {
      if (Number(secEnd[0]) !== 1) {
        return end()
      } else {
        return 'manyof'
      }
    } else {
      return end()
    }
  }
  return defineEnding()
}

export default productEnding




