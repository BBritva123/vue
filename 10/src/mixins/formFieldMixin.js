import BaseFormField from '@/components/BaseFormField.vue';
export default {
  components: {
    BaseFormField,
  },
  data() {
    return {

    }
  },
  props: {
    title: String,
    error: String,
    placeholder: String,
    value: String,
    type: {
      type: String,
      default: 'text',
    }
  },
  computed: {
    dataValue: {
      get() {
        return this.value
      },
      set(value) {
        this.$emit('input', value)
      }
    }
  }
}