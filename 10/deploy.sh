#!/usr/bin/env sh
# остановить публикацию при ошибках
set -e

# сборка приложения
npm run build

# переход в каталог сборки
cd build

# инициализация приложения и загрузка кода в github
git init
git add -A
git commit -m 'deploy'
git push -f https://github.com/BBritva123/tehnozavr-vue-app.git master:gh-pages