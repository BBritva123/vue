import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { API_BASE_URL } from '../../config'

Vue.use(Vuex)

const cartProducts = new Vuex.Store({
  state: {
    cartProducts: [],
    userAccessKey: null,
    cartProductsData: [],

    cartLoader: false,
    cartLoaderLoaded: false,
    cartLoaderFailed: false,

    cartIndicatorLoader: false,


  },

  mutations: {
    changeCartLoader(state, boolean) {
      state.cartLoader = boolean
    },
    changeCartLoaderLoaded(state, boolean) {
      state.cartLoaderLoaded = boolean
    },
    changeCartLoaderFailed(state, boolean) {
      state.cartLoaderFailed = boolean
    },
    changeCartIndicatorLoader(state, boolean) {
      state.cartIndicatorLoader = boolean
    },
    syncCartProducts(state) {
      state.cartProducts = state.cartProductsData.map(item => {
        return {
          productId: item.product.id,
          amount: item.quantity,
        }
      })
    },
    updateCartProductsData(state, items) {
      state.cartProductsData = items
    },
    updateUserAccessKey(state, accessKey) {
      state.userAccessKey = accessKey
    },
    updateCartProductAmount(state, { productId, amount }) {
      let item = state.cartProducts.find(item => item.productId == productId)
      if (item) {
        item.amount = amount
      } else {
        console.log('not item');
        state.cartProducts.push({ productId, amount })
      }
    },
    removeCartProduct(state, productId) {
      state.cartLoader = true
      state.cartLoaderField = false
        (new Promise(res => { res }))
        .then(() => {
          state.cartProducts = state.cartProducts.filter(item => item.productId !== productId)
          state.cartLoader = false
        })
        .catch(() => {
          state.cartLoader = false
          state.cartLoaderFailed = true
        })
    }
  },

  getters: {
    cartProd(state) {
      return state.cartProducts
    },
    cartInfoProducts(state, getters) {
      return getters.cartProd.map(item => {
        let prodCart = state.cartProductsData.find(p => p.product.id == item.productId).product
        return {
          ...item,
          product: {
            ...prodCart,
            path: prodCart.image.file.url
          }
        }
      })
    },
    cartTotalPrice(state, getters) {
      return getters.cartInfoProducts.reduce((accumulator, item) => (item.product.price * item.amount) + accumulator, 0)
    },
    productsCount(state, getters) {
      return getters.cartInfoProducts.reduce((acc, item) => acc + item.amount, 0)
    }
  },

  actions: {
    loadCart(context) {
      context.commit('changeCartLoader', true)
      context.commit('changeCartLoaderLoaded', false)
      context.commit('changeCartLoaderFailed', false)
      return (new Promise(resolve => setTimeout(resolve, 2000)))
        .then(() => {
          context.commit('changeCartLoader', true)
          return axios.get(API_BASE_URL + '/api/baskets', {
            params: {
              userAccessKey: context.state.userAccessKey
            }
          })
            .then(res => {
              if (!context.state.userAccessKey) {
                localStorage.setItem('userAccessKey', res.data.user.accessKey)
                context.commit('updateUserAccessKey', res.data.user.accessKey)
              }
              context.commit('updateCartProductsData', res.data.items)
              context.commit('syncCartProducts')
            })
        })
        .then(()=>{
          context.commit('changeCartLoader', false)
          context.commit('changeCartLoaderLoaded', true)
          setTimeout(()=>{
            context.commit('changeCartLoaderLoaded', false)
          },2000)
        })
        .catch(()=>{
          context.commit('changeCartLoader', false)
          context.commit('changeCartLoaderLoaded', false)
          context.commit('changeCartLoaderFailed', true)
        })
    },
    addToCart(context, { productId, amount }) {
      context.commit('changeCartIndicatorLoader', true)
      return (new Promise(resolve => setTimeout(resolve, 2000)))
        .then(() => {
          return axios.post(API_BASE_URL + '/api/baskets/products', {
            productId: productId,
            quantity: amount,
          },
            {
              params: {
                userAccessKey: context.state.userAccessKey,
              }
            })
            .then(res => {
              context.commit('updateCartProductsData', res.data.items)
              context.commit('syncCartProducts')
              context.commit('changeCartIndicatorLoader', false)

            })
            .catch(err => console.log('Error: add to cart'))
        })
        
    },
    deleteFromCart(context, { productId }) {
      context.commit('changeCartIndicatorLoader', true)
      return (new Promise(resolve => setTimeout(resolve, 2000)))
        .then(() => {
          return axios.delete(API_BASE_URL + '/api/baskets/products', {
            params: { userAccessKey: context.state.userAccessKey },
            data: { productId: productId }
          })
            .then(res => {
              context.commit('updateCartProductsData', res.data.items)
              context.commit('syncCartProducts')
              context.commit('changeCartIndicatorLoader', false)
            })
            .catch(error => {
              if (error.response) {
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
              } else if (error.request) {
                console.log(error.request);
              } else {
                console.log('Error', error.message);
              }
              console.log(error.config);
            });
        })

    },
    updateCartProductAmount(context, { productId, amount }) {
      context.commit('updateCartProductAmount', { productId, amount })
      if (!amount) return
      return axios.put(API_BASE_URL + '/api/baskets/products', { productId: productId, quantity: amount }, { params: { userAccessKey: context.state.userAccessKey } })
        .then((res) => {
          context.commit('updateCartProductsData', res.data.items)
        })
        .catch(() => {
          console.log('error actions updateProductAmount');
          context.commit('syncCartProducts')
        })
    },

  },
})

export default cartProducts

