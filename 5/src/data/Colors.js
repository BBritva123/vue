export default [
  {
    id:1,
    colorCode: 'rgb(115,182,234)',
  },
  {
    id:2,
    colorCode: 'rgb(255,190,21)',
  },
  {
    id:3,
    colorCode: 'rgb(147, 147, 147)',
  },
  {
    id:4,
    colorCode: 'rgb(139, 224, 0)',
  },
  {
    id:5,
    colorCode: 'rgb(255, 107, 0)',
  },
  {
    id:6,
    colorCode: 'rgb(255,255,255)',
  },
  {
    id:7,
    colorCode: 'rgb(0,0,0)',
  },
  {
    id:8,
    colorCode: '#f04c4c',
  },
]