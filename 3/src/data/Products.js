export default  [
    {
      title: 'Радионяня Motorola MBP16',
      price: 3690,
      path: 'img/radio.jpg'
    },
    {
      title: 'Ультразвуковая зубная щётка Playbrush Smart Sonic',
      price: 5660,
      path: 'img/toothbrush.jpg'
    },
    {
      title: 'Смартфон Xiaomi Mi Mix 3 6/128GB',
      price: 21790,
      path: 'img/phone.jpg'
    },
    {
      title: 'Электроскейт Razor Cruiser',
      price: 24690,
      path: 'img/board.jpg'
    },
    {
      title: 'Смартфон Xiaomi Mi A3 4/64GB Android One',
      price: 14960,
      path: 'img/phone-2.jpg'
    },
    {
      title: 'Смартфон Xiaomi Redmi 6/128GB',
      price: 8960,
      path: 'img/phone-3.jpg'
    },
    {
      title: 'Электрический дрифт-карт Razor Crazy Cart',
      price: 39900,
      path: 'img/bicycle.jpg'
    },
    {
      title: 'Гироскутер Razor Hovertrax 2.0',
      price: 34900,
      path: 'img/wheels.jpg'
    },
    {
      title: 'Детский трюковой самокат Razor Grom',
      price: 4990,
      path: 'img/scooter.jpg'
    },
    {
      title: 'Роллерсёрф Razor RipStik Air Pro',
      price: 6690,
      path: 'img/ripstik.jpg'
    },
    {
      title: 'Наушники AirPods с беспроводным зарядным футляром',
      price: 16560,
      path: 'img/airpods.jpg'
    },
    {
      title: 'Наушники Sony',
      price: 30690,
      path: 'img/headphones.jpg'
    },
  ]
