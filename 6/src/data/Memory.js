export default [
  {
    id:8,
    title: '8GB',
    isHave: true,
  },
  {
    id:16,
    title: '16GB',
    isHave: true,
  },
  {
    id:32,
    title: '32GB',
    isHave: true,
  },
  {
    id:64,
    title: '64GB',
    isHave: true,
  },
  {
    id:128,
    title: '128GB',
    isHave: true,
  },
  {
    id:256,
    title: '256GB',
    isHave: true,
  },
  {
    id:512,
    title: '512GB',
    isHave: true,
  },
  {
    id:1024,
    title: '1TB',
    isHave: true,
  },
]