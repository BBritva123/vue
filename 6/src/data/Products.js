export default [
  {
    id: 1,
    categoryId: 4,
    title: 'Радионяня Motorola MBP16',
    price: 3690,
    path: 'img/radio.jpg',
    colors: [
      {
        id: 1,
        colorCode: 'rgb(115,182,234)',
      },
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
      {
        id: 3,
        colorCode: 'rgb(147, 147, 147)',
      },
    ],
  },
  {
    id: 2,
    categoryId: 1,
    title: 'Ультразвуковая зубная щётка Playbrush Smart Sonic',
    price: 5660,
    path: 'img/toothbrush.jpg',
    colors: [
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
      {
        id: 3,
        colorCode: 'rgb(147, 147, 147)',
      },
      {
        id: 7,
        colorCode: 'rgb(0,0,0)',
      },
    ],

  },
  {
    id: 3,
    categoryId: 2,
    title: 'Смартфон Xiaomi Mi Mix 3 6/128GB',
    price: 21790,
    path: 'img/phone.jpg',
    colors: [
      {
        id: 5,
        colorCode: 'rgb(255, 107, 0)',
      },
      {
        id: 6,
        colorCode: 'rgb(255,255,255)',
      },
      {
        id: 7,
        colorCode: 'rgb(0,0,0)',
      },
    ],
    memory: [
      {
        id:32,
        title: '32GB',
        isHave: true,
      },
      {
        id:64,
        title: '64GB',
        isHave: true,
      },
      {
        id:128,
        title: '128GB',
        isHave: true,
      },
    ],
  },
  {
    id: 4,
    categoryId: 3,
    title: 'Электроскейт Razor Cruiser',
    price: 24690,
    path: 'img/board.jpg',
    colors: [
      {
        id: 3,
        colorCode: 'rgb(147, 147, 147)',
      },
      {
        id: 4,
        colorCode: 'rgb(139, 224, 0)',
      },
      {
        id: 5,
        colorCode: 'rgb(255, 107, 0)',
      },
    ],
  },
  {
    id: 5,
    categoryId: 2,
    title: 'Смартфон Xiaomi Mi A3 4/64GB Android One',
    price: 14960,
    path: 'img/phone-2.jpg',
    colors: [
      {
        id: 1,
        colorCode: 'rgb(115,182,234)',
        isHave:true,
      },
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
      {
        id: 7,
        colorCode: 'rgb(0,0,0)',
      },
      {
        id: 8,
        colorCode: '#f04c4c',
      },
    ],
  },
  {
    id: 6,
    categoryId: 2,
    title: 'Смартфон Xiaomi Redmi 6/128GB',
    price: 8960,
    path: 'img/phone-3.jpg',
    colors: [
      {
        id: 1,
        colorCode: 'rgb(115,182,234)',
      },
      {
        id: 4,
        colorCode: 'rgb(139, 224, 0)',
      },
      {
        id: 5,
        colorCode: 'rgb(255, 107, 0)',
      },
      {
        id: 8,
        colorCode: '#f04c4c',
      },
    ],
  },
  {
    id: 7,
    categoryId: 3,
    title: 'Электрический дрифт-карт Razor Crazy Cart',
    price: 39900,
    path: 'img/bicycle.jpg',
    colors: [
      {
        id: 5,
        colorCode: 'rgb(255, 107, 0)',
      },
      {
        id: 8,
        colorCode: '#f04c4c',
      },
    ],
  },
  {
    id: 8,
    categoryId: 3,
    title: 'Гироскутер Razor Hovertrax 2.0',
    price: 34900,
    path: 'img/wheels.jpg',
    colors: [
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
      {
        id: 4,
        colorCode: 'rgb(139, 224, 0)',
      },
      {
        id: 6,
        colorCode: 'rgb(255,255,255)',
      },
    ],
  },
  {
    id: 9,
    categoryId: 3,
    title: 'Детский трюковой самокат Razor Grom',
    price: 4990,
    path: 'img/scooter.jpg',
    colors: [
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
    ]
  },
  {
    id: 10,
    categoryId: 3,
    title: 'Роллерсёрф Razor RipStik Air Pro',
    price: 6690,
    path: 'img/ripstik.jpg',
    colors: [
      {
        id: 1,
        colorCode: 'rgb(115,182,234)',
      },
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
      {
        id: 6,
        colorCode: 'rgb(255,255,255)',
      },
    ],
  },
  {
    id: 11,
    categoryId: 5,
    title: 'Наушники AirPods с беспроводным зарядным футляром',
    price: 16560,
    path: 'img/airpods.jpg',
    colors: [
      {
        id: 1,
        colorCode: 'rgb(115,182,234)',
      },
      {
        id: 7,
        colorCode: 'rgb(0,0,0)',
      },
      {
        id: 8,
        colorCode: '#f04c4c',
      },
    ],
  },
  {
    id: 12,
    categoryId: 5,
    title: 'Наушники Sony',
    price: 30690,
    path: 'img/headphones.jpg',
    colors: [
      {
        id: 1,
        colorCode: 'rgb(115,182,234)',
      },
      {
        id: 2,
        colorCode: 'rgb(255,190,21)',
      },
      {
        id: 7,
        colorCode: 'rgb(0,0,0)',
      },
      {
        id: 8,
        colorCode: '#f04c4c',
      },
    ],
  },
]
