import Vue from 'vue'
import Vuex from 'vuex'
import products from '@/data/Products'

Vue.use(Vuex)

const cartProducts = new Vuex.Store({
  state: {
    cartProducts: [],
  },
  mutations: {
    addProductToCart(state, { productId, amount }) {
      let item = state.cartProducts.find(item => item.productId === productId)
      if (item) {
        item.amount += amount
      } else {
        state.cartProducts.push({ productId, amount })
      }
    },
    updateCartProductAmount(state, { productId, amount }) {
      let item = state.cartProducts.find(item => item.productId == productId)
      if (item) {
        item.amount = amount
      } else {
        console.log('not item');
        state.cartProducts.push({ productId, amount })
      }
    },
    removeCartProduct(state, productId) {
      state.cartProducts = state.cartProducts.filter(item => item.productId !== productId)
    }
  },
  getters: {
    cartProd(state) {
      return state.cartProducts
    },
    cartInfoProducts(state) {
      return state.cartProducts.map(item => {
        return {
          ...item,
          product: products.find(product => product.id === item.productId)
        }
      })
    },
    cartTotalPrice(state, getters) {
      return getters.cartInfoProducts.reduce((accumulator, item) => (item.product.price * item.amount) + accumulator, 0)
    },
    productsCount(state, getters) {
      return getters.cartInfoProducts.reduce((acc, item) => acc + item.amount, 0)
    }
  },
})

export default cartProducts

